import React, { useContext } from "react";
import { CartContext } from "../Context/Cart-context";
import "./Cart.css";
const Cart = () => {
  const [cartItems, setCartItems] = useContext(CartContext);
  const [remItem, setRemItem] = useContext(CartContext);

  const total = cartItems.reduce((accumulator, current) => {
    return Number(accumulator) + Number(current.price);
  }, 0);

  return (
    <div className="box">
      <p>Item is cart : {cartItems.length}</p>
      <p>Total Price : {total}</p>
    </div>
  );
};

export default Cart;
