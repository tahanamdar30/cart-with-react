import React, { useContext } from "react";
import "./Product.css";
import { CartContext } from "../Context/Cart-context";
//Create Product in this file and move to productList
//child of product-list
const Product = (props) => {
  const [cartItems, setCartItems] = useContext(CartContext);
  const [remItem, setRemItem] = useContext(CartContext);

  const product = { title: props.title, price: props.price };
  const addToCart = () => {
    setCartItems((input) => {
      return [...input, product];
    });
  };

  const remove = (varietalCount) => {
    // const newItems = remItem.filter((item) => item.id !== varietalCount.id);
    // setRemItem(newItems);
    setCartItems((cartItem) => {
      if (cartItem.length > 0) {
        var out = [];
        var flag = false;
        cartItem.map((item) => {
          if (flag === true && item.title === product.title) {
            out = [...out, item];
          } else if (item.title !== product.title) {
            out = [...out, item];
          } else {
            flag = true;
          }
        });
        cartItem = out;
      }
      return cartItem;
    });
  };

  return (
    <div className="product">
      <p>{props.title}</p>
      <p>{props.price}</p>
      <button onClick={addToCart}>Add To Cart</button>
      <button onClick={remove}>Remove from Cart</button>
    </div>
  );
};

export default Product;
