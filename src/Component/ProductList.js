import React from "react";
import "./ProductList.css";
import Product from "./Product";

const ProductList = () => {
  // Array of productList but in real project we get the api
  const products = [
    { id: "1", title: "Samsung", price: "132000" },
    { id: "2", title: "Apple", price: "532000" },
    { id: "3", title: "Huawei", price: "332000" },
    { id: "4", title: "Nokia", price: "232000" },
  ];

  return (
    <div className="productList">
      {products.map((item) => {
        //pass to the products from list
        return <Product key={item.id} title={item.title} price={item.price} />;
      })}
    </div>
  );
};

export default ProductList;
