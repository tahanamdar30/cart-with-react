import React from "react";
import "./App.css";
//show ProductList at this file
import ProductList from "./Component/ProductList";
import Cart from "./Component/Cart";
//import context
import { CartProvider } from "./Context/Cart-context";

const App = () => {
  return (
    <CartProvider>
      <div className="main">
        <header>
          <h2>CartApp</h2>
        </header>
        <ProductList />
        <Cart />
      </div>
    </CartProvider>
  );
};

export default App;
